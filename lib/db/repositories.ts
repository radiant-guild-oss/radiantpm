import makeRepository from "./make-repository";
import connection from "./connection";
import {Feed, Maintainer, Package, Version} from "./orm";

export const feed = makeRepository(connection, Feed);
export const maintainer = makeRepository(connection, Maintainer);
export const packg = makeRepository(connection, Package);
export const version = makeRepository(connection, Version);
