import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn
} from "typeorm";

export enum FeedFormat {
    npm,
    NuGet
}

@Entity()
export class Maintainer {
    @PrimaryGeneratedColumn()
    id: string;

    @Column({length: 24})
    name: string;

    @Column({length: 48})
    email: string;

    @OneToMany(type => Version, version => version.author)
    authoredVersions: any[]; // must be any, see comment below

    @OneToMany(type => Version, version => version.publisher)
    publishedVersions: any[]; // must be any, see comment below

    constructor(name: string, email: string) {
        this.name = name;
        this.email = email;
    }
}

@Entity()
export class Version {
    @PrimaryGeneratedColumn()
    id: string;

    @Column({length: 128})
    description: string;

    @Column({length: 16})
    versionName: string;

    @Column("simple-array")
    keywords: string[];

    @ManyToOne(type => Maintainer, {eager: true})
    author: Maintainer;

    @ManyToOne(type => Maintainer, {eager: true})
    publisher: Maintainer;

    @ManyToOne(type => Package, pkg => pkg.versions)
    package: any; // cannot be Package type, as reflection accesses that before it is defined

    @Column({nullable: true})
    repository?: string;

    @Column({nullable: true})
    bugsUrl?: string;

    @Column({nullable: true})
    homepage?: string;

    @Column("simple-array")
    licenses: string[];

    @Column()
    readme: string;

    @Column()
    readmeFilename: string;

    @Column({length: 64})
    sha: string;

    @Column({default: new Date()})
    publishDate: Date;
}

@Entity()
export class Package {
    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    name: string;

    @OneToOne(type => Version)
    @JoinColumn()
    latestVersion: Version;

    @OneToMany(type => Version, version => version.package)
    versions: Version[];

    @ManyToOne(type => Feed, feed => feed.packages)
    feed: any; // cannot be feed type. see same comment above

    @Column()
    created: Date = new Date();

    @Column()
    modified: Date = new Date();

    constructor(name: string) {
        this.name = name;
    }
}

@Entity()
export class Feed {
    @PrimaryColumn()
    id: string;

    @Column("integer")
    format: FeedFormat;

    @Column({nullable: true})
    group: string;

    @Column({nullable: true})
    description: string;

    @OneToMany(type => Package, pkg => pkg.feed)
    packages: Package[];

    constructor(name: string, format: FeedFormat, group?: string) {
        this.id = name;
        this.format = format;
        this.group = group;
    }
}
