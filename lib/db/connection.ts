import {createConnection} from "typeorm";
import "reflect-metadata";
import {Feed, Maintainer, Package, Version} from "./orm";

export const connectionName = "CONN_" + Math.ceil(Math.random() * 0xffffff).toString(16);
console.log("Connection name:", connectionName);

export default createConnection({
    type: "postgres",
    url: process.env.PG_CONNECTION_STRING,
    entities: [
        Maintainer,
        Feed,
        Package,
        Version
    ],
    synchronize: true,
    logging: false,
    name: connectionName
});
