import {
    Connection,
    ObjectType,
    EntitySchema
} from "typeorm";

export default function <Entity>(connection: Promise<Connection>,
                                 entityClass: ObjectType<Entity> | EntitySchema<Entity> | string) {
    return connection.then(conn => conn.getRepository(entityClass));
}
