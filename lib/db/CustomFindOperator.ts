import {Connection, FindOperator} from "typeorm/index";

export interface CFOCallback {
    /**
     * Gets the SQL needed to run this query
     * @param alias - Alias of the column name
     * @param parameters - List of input parameters
     */
    (alias: string, parameters: string[]): string;
}

export default class CustomFindOperator extends FindOperator<any> {
    constructor(private callback: CFOCallback, parameters: (number | string)[] = []) {
        super(null, parameters, parameters.length > 0, parameters.length > 0);
    }

    toSql(connection: Connection, aliasPath: string, parameters: string[]): string {
        return this.callback(aliasPath, parameters);
    }
}
