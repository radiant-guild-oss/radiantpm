import {GetServerSidePropsContext, NextApiRequest, NextApiResponse} from "next";

export enum ServiceLifetime {
    // Created on every usage
    Transient,

    // Created per request
    Scoped,

    // Only created once (lazily)
    Singleton
}

export type ContextType = GetServerSidePropsContext | {
    req: NextApiRequest,
    res: NextApiResponse
};

export type Constructor<T> = {new(...args: any[]): T};
export type Creator<T> = (ctx: ContextType) => T;

interface ServiceInfo<T> {
    lifetime: ServiceLifetime;
    creator: Creator<T>
}

const mapping: Map<Constructor<any>, ServiceInfo<any>> = new Map();
const scopedMappings: Map<Constructor<any>, Map<ContextType, any>> = new Map();

function removeScopedMapping(type: Constructor<any>, ctx: ContextType) {
    if (!scopedMappings.has(type)) return;
    const mapping = scopedMappings.get(type);
    if (!mapping.has(ctx)) return;
    mapping.delete(ctx);
    if (mapping.size === 0) scopedMappings.delete(type);
}

export function register<T>(type: Constructor<T>, lifetime: ServiceLifetime, creator: Creator<T>) {
    if (mapping.has(type)) console.warn("Already registered, will override");
    mapping.set(type, {
        lifetime,
        creator
    });
}

export function inject<J>(ctx: ContextType, type: Constructor<J>): J {
    if (!mapping.has(type)) throw new ReferenceError("Service does not exist");
    const {lifetime, creator} = mapping.get(type);

    switch (lifetime) {
        case ServiceLifetime.Transient:
            return creator(ctx);
        case ServiceLifetime.Scoped:
            if (!scopedMappings.has(type)) scopedMappings.set(type, new Map());
            const scopedMapping = scopedMappings.get(type);
            if (!scopedMapping.has(ctx)) scopedMapping.set(ctx, creator(ctx));
            ctx.res.on("finish", () => removeScopedMapping(type, ctx));
            ctx.res.on("error", () => removeScopedMapping(type, ctx));
            return scopedMapping.get(ctx);
        case ServiceLifetime.Singleton:
            if (!scopedMappings.has(type)) scopedMappings.set(type, new Map());
            const singletonMapping = scopedMappings.get(type);
            if (!singletonMapping.has(ctx)) singletonMapping.set(ctx, creator(ctx));
            return singletonMapping.get(ctx);
    }
}
