import settings from "../settings.json";

export interface Settings {
    name: string;
    branding: boolean;
    allowedGroups: string[];
}

export default settings as Settings;
