export enum AccessLevel {
    None = 0,
    Guest = 10,
    Reporter = 20,
    Developer = 30,
    Maintainer = 40,
    Owner = 50
}

export interface GetGroupsItem {
    id: number;
    name: string;
    path: string;
    description: string;
    visibility: "public" | "private";
    share_with_group_lock: boolean;
    require_two_factor_authentication: boolean;
    two_factor_grace_period: number;
    project_creation_level: string;
    auto_devops_enabled: boolean;
    subgroup_creation_level: string;
    emails_disabled: boolean;
    mentions_disabled: boolean;
    lfs_enabled: boolean;
    default_branch_protection: number;
    avatar_url: string;
    web_url: string;
    request_access_enabled: false;
    full_name: string;
    full_path: string;
    file_template_project_id: number;
    parent_id: number;
    created_at: string;
}

interface GetGroupsOptions {
    skip_groups?: number[];
    all_available?: boolean;
    search?: string;
    order_by?: string;
    sort?: "asc" | "desc";
    statistics?: boolean;
    with_custom_attributes?: boolean;
    owned?: boolean;
    min_access_level?: AccessLevel;
    top_level_only?: boolean;
}

export interface User {
    id: number;
    username: string;
    email: string;
    name: string;
    state: "active" | "inactive";
    avatar_url: string;
    web_url: string;
    created_at: string;
    bio: string;
    location: string;
    public_email: string;
    skype: string;
    linkedin: string;
    twitter: string;
    website_url: string;
    organization: string;
    job_title: string;
}

export interface SudoUser extends User {
    last_sign_in_at: string;
    confirmed_at: string;
    theme_id: number;
    last_activity_on: string;
    color_scheme_id: number;
    projects_limit: number;
    current_sign_in_at: string;
    identities: {
        provider: string;
        extern_uid: string;
    }[];
    can_create_group: boolean;
    can_create_project: boolean;
    two_factor_enabled: boolean;
    external: boolean;
    private_profile: boolean;
}

export interface Project {
    id: number;
    description: string;
    default_branch: string;
    visibility: "public" | "internal" | "private";
    ssh_url_to_repo: string;
    http_url_to_repo: string;
    web_url: string;
    readme_url: string;
    tag_list: string[];
    owner: {id: number, name: string, created_at: string},
    name: string;
    name_with_namespace: string;
    path: string;
    path_with_namespace: string;
    issues_enabled: boolean;
    open_issues_count: number;
    merge_requests_enabled: boolean;
    jobs_enabled: boolean;
    wiki_enabled: boolean;
    snippets_enabled: boolean;
    can_create_merge_request_in: boolean;
    resolve_outdated_diff_discussions: boolean;
    container_registry_enabled: boolean;
    container_expiration_policy: {
        cadence: string;
        enabled: boolean;
        keep_n: null;
        older_than: string;
        name_regex_delete: string;
        name_regex_keep: string;
        next_run_at: string;
    };
    created_at: string;
    last_activity_at: string;
    creator_id: string;
    namespace: {
        id: string;
        name: string;
        path: string;
        kind: string;
        full_path: string;
        avatar_url: string;
        web_url: string;
    };
    import_status: string;
    import_error: string;
    permissions: {
        project_access: {
            access_level: AccessLevel;
            notification_level: number;
        };
        group_access: {
            access_level: AccessLevel;
            notification_level: number;
        };
    };
    archived: boolean;
    avatar_url: string;
    license_url: string;
    license: {
        key: string;
        name: string;
        nickname: string;
        html_url: string;
        source_url: string;
    };
    shared_runners_enabled: boolean;
    forks_count: number;
    star_count: number;
    runners_token: string;
    ci_default_git_depth: number;
    public_jobs: boolean;
    shared_with_groups: {
        group_id: number;
        group_name: string;
        group_full_path: string;
        group_access_level: number;
    }[];
    repository_storage: string;
    only_allow_merge_if_pipeline_succeeds: boolean;
    allow_merge_on_skipped_pipeline: boolean;
    only_allow_merge_if_all_discussions_are_resolved: boolean;
    remove_source_branch_after_merge: boolean;
    printing_merge_requests_link_enabled: boolean;
    request_access_enabled: boolean;
    merge_method: string;
    auto_devops_enabled: boolean;
    auto_devops_deploy_strategy: string;
    approvals_before_merge: number;
    mirror: boolean;
    mirror_user_id: number;
    mirror_trigger_builds: boolean;
    only_mirror_protected_branches: boolean;
    mirror_overwrites_protected_branches: boolean;
    external_authorization_classification_label: string;
    packages_enabled: boolean;
    service_desk_enabled: boolean;
    service_desk_address: string;
    autoclose_referenced_issues: boolean;
    suggestion_commit_message: string;
    marked_for_deletion_on: string;
    compliance_frameworks: string[];
    statistics: {
        commit_count: number;
        storage_size: number;
        repository_size: number;
        wiki_size: number;
        lfs_objects_size: number;
        job_artifacts_size: number;
        packages_size: number;
        snippets_size: number;
    };
    _links: {
        self: string;
        issues: string;
        merge_requests: string;
        repo_branches: string;
        labels: string;
        events: string;
        members: string;
    };
}

export default class GitlabApi {
    static isValidAccessToken(accessToken: string): Promise<boolean> {
        const api = new GitlabApi(accessToken);
        return api.checkAccessToken();
    }

    constructor(private accessToken: string, private base: string = "https://gitlab.com/api/v4/") {}

    private makeUrl(url: string, data: Record<string, string>) {
        const result = new URL(url, this.base);
        const searchParams = new URLSearchParams(data);
        return result.toString() + "?" + searchParams.toString();
    }

    private request(path: string, data: any) {
        console.debug("Fetching", this.makeUrl(path, data));
        return fetch(this.makeUrl(path, data).toString(), {
            headers: {
                "Private-Token": this.accessToken
            }
        })
            .then(res => new Promise<Response>((yay, nay) =>
                res.status < 400 ?
                    yay(res) : nay(res.status)))
            .then(res => res.json());
    }

    checkAccessToken(): Promise<boolean> {
        return this.getUser().then(() => true).catch(() => false);
    }

    getGroups(options: GetGroupsOptions = {}): Promise<GetGroupsItem[]> {
        return this.request("groups", options);
    }

    getProject(id: number | string, options: {
        statistics?: boolean,
        license?: boolean,
        with_custom_attributes?: boolean
    } = {}): Promise<Project> {
        return this.request(`projects/${encodeURIComponent(id)}`, options);
    }

    getUser(): Promise<SudoUser>;
    getUser(id: string): Promise<User>;
    getUser(id?: string): Promise<User> {
        return this.request(id ? `users/${id}` : "user", {});
    }
}
