export interface ControlledInputProps<T> {
    value: T;

    name?: string;
    disabled?: boolean;
    loading?: boolean;

    onChange?(newValue: T, name?: string): void;
}

export enum Severity {
    Warning,
    Error
}

export interface Diagnostic {
    message: string;
    type: string;
    severity: Severity;
}

export type Validator<T> = (value: T) => Diagnostic[];

export interface ShowsDiagnostics<T> {
    validator: Validator<T>;
}

export function getDiagnostics<T>(props: ControlledInputProps<T> & ShowsDiagnostics<T>) {
    return props.validator(props.value);
}

interface ValidatorCombinerOptions {
    [name: string]: Validator<any>;
}

type ValidatorCombinerResult<T extends ValidatorCombinerOptions> = {
    [key in keyof T]: any;
};

export function combineValidators<T extends ValidatorCombinerOptions>(validators: T): Validator<ValidatorCombinerResult<T>> {
    const keys: (keyof T)[] = Object.keys(validators);
    return value => keys.reduce((prev: Diagnostic[], curr) =>
        [...prev, ...validators[curr](value[curr])], [])
}

export function isValid<T>(validator: Validator<T>, source: T) {
    return validator(source).length === 0;
}
