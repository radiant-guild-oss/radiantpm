import ILoggedIn from "./cookie/ILoggedIn";
import {parseCookies} from "nookies";
import {NextApiRequest, NextPageContext} from "next";
import GitlabApi from "./GitlabApi";

export default function isLoggedIn(nookies: any): nookies is ILoggedIn {
    return "accessToken" in nookies;
}

export async function isValidUser(ctx: Pick<NextPageContext, "req"> | {req: NextApiRequest}) {
    const cookies = parseCookies(ctx);
    if (!isLoggedIn(cookies)) return false;
    return GitlabApi.isValidAccessToken(cookies.accessToken);

}
