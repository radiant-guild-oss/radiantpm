import {InterpolationWithTheme} from "@emotion/core";

declare interface Attributes {
    css?: InterpolationWithTheme<any>
}
