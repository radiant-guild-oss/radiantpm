import {NextApiRequest, NextApiResponse} from "next";
import {packg, version} from "../../../../../../../../../lib/db/repositories";
import setupServices from "../../../../../../../../../lib/setupServices";
import CustomFindOperator from "../../../../../../../../../lib/db/CustomFindOperator";
import canViewPackage from "../../../../../../../../../lib/permissions/canViewPackage";
import {config} from "dotenv";

config();
setupServices();

export default async function (req: NextApiRequest, res: NextApiResponse) {
    const pkg = await packg.then(pkg => pkg.findOne({
        where: {
            name: new CustomFindOperator((alias, [pkgName]) => `LOWER(${alias}) = ${pkgName}`,
                [(req.query.package as string).toLowerCase()])
        },
        relations: ["feed"]
    }));

    if (!pkg || !await canViewPackage({req, res}, pkg)) {
        res.status(404).json({
            error: "Not found"
        });
        return;
    }

    const requestedVersion = await version.then(ver => ver.findOne({
        where: {
            versionName: req.query.version as string,
            package: pkg
        }
    }));

    if (!requestedVersion) {
        res.status(404).json({
            error: "Not found"
        });
        return;
    }

    const sha = requestedVersion.sha;

    if (req.url.endsWith(".nupkg")) {
        res.status(302);
        res.setHeader("Location", new URL(`${sha}.nupkg`, process.env.CDN_PUBLIC_PATH).toString());
        res.end();
    } else if (req.url.endsWith(".nuspec")) {
        res.status(302);
        res.setHeader("Location", new URL(`${sha}.nuspec`, process.env.CDN_PUBLIC_PATH).toString());
        res.end();
    } else {
        res.status(404).json({
            error: "Not found"
        });
    }
}
