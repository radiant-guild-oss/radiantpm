import {NextApiRequest, NextApiResponse} from "next";
import {extract} from "tar-stream";
import gunzip from "gunzip-maybe";
import {Buffer} from "buffer";
import {createHash} from "crypto";
import got from "got";
import {config} from "dotenv";
import S3 from "aws-sdk/clients/s3";
import {join} from "path";
import {feed, maintainer, packg, version} from "../../../../lib/db/repositories";
import {Feed, FeedFormat, Maintainer, Package, Version} from "../../../../lib/db/orm";
import canViewPackage from "../../../../lib/permissions/canViewPackage";
import GitlabApi, {AccessLevel} from "../../../../lib/GitlabApi";

config();

function readTgzFile(path: string, stream: NodeJS.ReadableStream): Promise<Buffer> {
    return new Promise((yay, nay) => {
        const extractor = extract();
        let foundFile = false;

        extractor.on("entry", (header, stream, next) => {
            let data: Buffer[] = [];
            foundFile = true;

            if (header.name !== path) {
                stream.resume();
                return next();
            }

            stream.on("data", dat => {
                data.push(dat);
            });

            stream.on("end", () => {
                const buff = Buffer.concat(data);
                yay(buff);
            });
        });

        extractor.on("finish", () => {
            if (!foundFile) nay(new Error("File not found"));
        });

        extractor.on("error", nay);

        stream.pipe(gunzip()).pipe(extractor);
    });
}

async function handleNpmPackage(req: NextApiRequest, res: NextApiResponse, pkg: Package) {
    let latestVersionPackage: any = {keywords: []};
    res.status(200).json({
        _id: pkg.name,
        time: {
            created: pkg.created,
            modified: pkg.modified,
            ...Object.fromEntries(pkg.versions.map(version => [version.versionName, version.publishDate]))
        },
        name: pkg.name,
        "dist-tags": {
            latest: pkg.latestVersion.versionName
        },
        versions: Object.fromEntries(await Promise.all(pkg.versions.map(async version => {
            const tarballPath = process.env.CDN_PUBLIC_PATH + version.sha + ".tgz";
            const tarballStream = got.stream(tarballPath);
            const tarballStreamSource: Buffer[] = [];
            tarballStream.on("data", dat => tarballStreamSource.push(dat));

            let tarballStreamFinished = false;
            tarballStream.on("end", () => tarballStreamFinished = true);

            const file = (await readTgzFile("package/package.json", tarballStream)).toString("utf8");
            if (!tarballStreamFinished) await new Promise(yay => tarballStream.on("end", yay));

            const tarballStreamBuff = Buffer.concat(tarballStreamSource);

            const json = JSON.parse(file);
            if (version.versionName === pkg.latestVersion.versionName) latestVersionPackage = json;

            const integrity = createHash("sha512");
            integrity.update(tarballStreamBuff);

            const shasum = createHash("sha1");
            shasum.update(tarballStreamBuff);

            return [version.versionName, {
                ...json,
                name: pkg.name,
                version: version.versionName,
                description: version.description,
                repository: version.repository ? {
                    type: version.repository.split(/(?::\/\/)|(?:\+)/, 1)[0],
                    url: version.repository
                } : null,
                author: {
                    name: version.author.name,
                    email: version.author.email
                },
                bugs: {
                    url: version.bugsUrl
                },
                homepage: version.homepage,
                _id: `${pkg.name}@${version.versionName}`,
                dist: {
                    integrity: "sha512-" + integrity.digest().toString("base64"),
                    shasum: shasum.digest().toString("hex"),
                    tarball: tarballPath
                }
            }];
        }))),
        homepage: pkg.latestVersion.homepage,
        keywords: latestVersionPackage.keywords,
        repository: pkg.latestVersion.repository ? {
            type: pkg.latestVersion.repository.split(/(?::\/\/)|(?:\+)/, 1)[0],
            url: pkg.latestVersion.repository
        } : null,
        author: {
            name: pkg.latestVersion.author.name,
            email: pkg.latestVersion.author.email
        },
        bugs: {
            url: pkg.latestVersion.bugsUrl
        },
        licenses: pkg.latestVersion.licenses,
        readme: pkg.latestVersion.readme,
        readmeFilename: pkg.latestVersion.readmeFilename
    });
}

interface NpmUploadBody {
    _id: string;
    name: string;
    description: string;
    "dist-tags": {latest: string};
    versions: {
        [key: string]: {
            name: string;
            version: string;
            description: string;
            main: string;
            scripts: {
                [key: string]: string;
            };
            author: string;
            license: string;
            dependencies: {
                [key: string]: string;
            };
            readme: string;
            _id: string;
            _nodeVersion: string;
            _npmVersion: string;
            dist: {
                integrity: string;
                shasum: string;
                tarball: string;
            }
        };
    };
    readme: string;
    _attachments: {
        [key: string]: {
            content_type: string;
            data: string;
            length: number;
        };
    };
}

async function uploadNpmPackage(req: NextApiRequest, res: NextApiResponse, feed: Feed, packageName: string, api: GitlabApi) {
    const body = req.body as NpmUploadBody;

    const attachment = body._attachments[Object.keys(body._attachments)[0]];

    const verName = body["dist-tags"].latest;
    const theirVersion = body.versions[verName];

    const tempPkg = await packg.then(f => f.findOne({
        where: {
            feed: {
                id: feed.id
            },
            name: packageName
        },
        relations: ["latestVersion", "versions"]
    }));

    // check the version doesn't already exist
    if (tempPkg && tempPkg.versions.some(it => it.versionName === verName)) {
        return res.status(409).json({
            error: "Version already exists"
        });
    }

    const data = new Buffer(attachment.data, "base64");
    const shaCalc = createHash("sha1");
    shaCalc.update(data);
    const sha = shaCalc.digest().toString("hex");

    const storage = new S3({
        credentials: {
            accessKeyId: process.env.ACCESS_KEY_ID,
            secretAccessKey: process.env.ACCESS_KEY_SECRET
        },
        endpoint: process.env.S3_ENDPOINT
    });

    const upload = storage.upload({
        Bucket: process.env.CDN_BUCKET_NAME,
        Key: join(process.env.CDN_NAMESPACE, sha + ".tgz"),
        Body: data,
        ACL: "public-read"
    });

    await upload.promise();

    const pkg = tempPkg || (() => {
        const pkg = new Package(packageName);
        pkg.feed = feed;
        return pkg;
    })();

    pkg.modified = new Date();
    await packg.then(pk => pk.save(pkg));

    const user = await api.getUser();
    const publisher = await version.then(v => v.findOneOrFail({
        where: {
            package: {
                id: pkg.id
            }
        },
        relations: ["publisher"]
    })).then(r => r.publisher).catch(() =>
        maintainer.then(m => m.findOneOrFail({
            where: {
                email: user.email
            }
        }))
    ).catch(async () => {
        const pub = new Maintainer(user.name, user.email);
        await maintainer.then(m => m.save(pub));
        return pub;
    });

    const ourVersion = new Version();
    ourVersion.description = theirVersion.description;
    ourVersion.versionName = verName;
    ourVersion.author = publisher;
    ourVersion.publisher = publisher;
    ourVersion.package = pkg.id;
    ourVersion.licenses = [theirVersion.license];
    ourVersion.keywords = [];
    ourVersion.readme = body.readme;
    ourVersion.readmeFilename = "README.md"; // TODO
    ourVersion.sha = sha;
    ourVersion.publishDate = new Date();

    await version.then(v => v.save(ourVersion));
    pkg.latestVersion = ourVersion;
    await packg.then(pk => pk.save(pkg));
    await version.then(v => v.save(ourVersion));

    res.status(201).json({});
}

const authErrorMessage = "The package might not exist or you might not have access to it";

export default async (req: NextApiRequest, res: NextApiResponse) => {
    const feedName = req.query.feed as string, name = (req.query.name as string[]).join("/");

    // make sure the user can access the package
    const token = req.headers.authorization?.split(" ", 2)?.[1];
    if (!token) return res.status(401).json({
        error: "You need to `npm login`"
    });

    const api = new GitlabApi(token);

    try {
        const feedObj = await feed.then(fd => fd.findOne({id: feedName}));

        switch (req.method) {
            case "GET": {
                const pkg = await packg.then(pk => pk.findOne({
                    where: {
                        feed: {
                            id: feedName
                        },
                        name
                    },
                    relations: ["latestVersion", "versions", "feed"]
                }));
                if (!pkg) throw new ReferenceError("Could not find package");

                if (!await canViewPackage({req, res}, pkg, api)) {
                    return res.status(404).json({
                        error: authErrorMessage
                    });
                }

                switch ((pkg.feed as Feed).format) {
                    case FeedFormat.npm:
                        await handleNpmPackage(req, res, pkg);
                        break;
                }
                break;
            }
            case "PUT": {
                const canView = await canViewPackage({req, res}, {
                    name,
                    feed: feedObj
                }, api, AccessLevel.Maintainer, true);

                if (canView === null) {
                    return res.status(404).json({
                        error: "Gitlab repository doesn't exist"
                    })
                }

                if (!canView) {
                    return res.status(401).json({
                        error: "Must be at least maintainer to publish"
                    });
                }

                switch (feedObj.format) {
                    case FeedFormat.npm:
                        await uploadNpmPackage(req, res, feedObj, name, api);
                        break;
                }
            }
        }
    } catch (err) {
        res.status(404).json({
            error: process.env.NODE_END === "production" ? authErrorMessage : err.message
        });
        console.error(err);
    }
};
