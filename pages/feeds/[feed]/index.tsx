import {GetServerSideProps, InferGetServerSidePropsType} from "next";
import {Box, Heading, Text} from "@chakra-ui/core";
import humanize from "@alduino/humanizer/dist/dateTime";
import DefaultDateTimeHumanizeStrategy from "@alduino/humanizer/dist/dateTime/DefaultDateTimeHumanizeStrategy";
import Error from "next/error";
import {feed, packg} from "../../../lib/db/repositories";
import {Package} from "../../../lib/db/orm";
import canViewPackage from "../../../lib/permissions/canViewPackage";
import {Buttons, ExtraInfo, LinkListItem} from "../../../components/LinkListItem";
import Link from "../../../components/Link";

export const getServerSideProps: GetServerSideProps = async ctx => {
    const feedName = ctx.params.feed as string;

    const count = await feed.then(fd => fd.count({
        where: {id: feedName}
    }));
    const notFound = count === 0;

    if (notFound) {
        ctx.res.statusCode = 404;
    }

    const items: Package[] = await packg.then(pk => pk.find({
        where: {
            feed: {
                id: feedName
            }
        },
        relations: ["latestVersion", "versions", "feed"]
    })).catch(() => []);

    const visibleItems =
        (await Promise.all(
            items.map(item => canViewPackage(ctx, item).then(res => [item, res]))))
        .filter(it => it[1]).map(it => it[0]);

    return {
        props: {
            feedName,
            notFound,
            items: JSON.parse(JSON.stringify(visibleItems))
        }
    };
};

export default function Feed(props: InferGetServerSidePropsType<typeof getServerSideProps>) {
    if (props.notFound) {
        return (
            <Error statusCode={404} />
        );
    }

    return (
        <Box>
            <Heading>{props.feedName} feed</Heading>

            {props.items.map((item: Package) => (
                <LinkListItem
                    title={item.name}
                    link="/feeds/[feed]/[...name]"
                    linkAs={`/feeds/${props.feedName}/${item.name}`}
                    detail={`v${item.latestVersion.versionName}`}
                    description={item.latestVersion.description}
                    key={item.id}
                >
                    <ExtraInfo>
                        <Text fontSize="sm">last updated {humanize(new Date(item.latestVersion.publishDate), void 0, new DefaultDateTimeHumanizeStrategy())}</Text>
                    </ExtraInfo>
                    <Buttons />
                </LinkListItem>
            ))}
            {props.items.length === 0 && (
                <Box mt={4} color="gray.500">
                    <Text textAlign="center">Nothing to see here...</Text>
                    <Text textAlign="center">
                        You might need to <Link href="/login" query={{redirect: process.browser && location.href}}>log in</Link> to see this feed.
                    </Text>
                </Box>
            )}
        </Box>
    );
}
