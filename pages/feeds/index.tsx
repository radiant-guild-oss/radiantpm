import {Box, Heading, Icon, Skeleton, Stack, Text} from "@chakra-ui/core";
import {useAsyncEffect} from "@react-hook/async";
import {pluralize, singularize} from "@alduino/humanizer/string";
import {GetServerSideProps} from "next";
import {Feed, FeedFormat} from "../../lib/db/orm";
import {Buttons, ExtraInfo, LinkListItem} from "../../components/LinkListItem";
import {inject} from "../../lib/dependency-injection";
import GitlabApi, {AccessLevel} from "../../lib/GitlabApi";
import Link from "../../components/Link";

function count(number: number, word: string) {
    if (number === 1) return number + " " + singularize(word, false);
    return number + " " + pluralize(word, false);
}

interface Props {
    canAddFeed: boolean;
}

export default function Feeds(props: Props) {
    const {value: feeds, status, error} = useAsyncEffect(() =>
        fetch("/api/feeds")
            .then(res => res.json() as Promise<Feed[]>),
        []
    );

    return (
        <Box>
            <Heading>Feeds</Heading>

            {status === "success" ? (
                feeds.length > 0 ?
                    feeds.map(feed => (
                        <LinkListItem
                            title={feed.id} detail={"(" + FeedFormat[feed.format] + ")"} key={feed.id} description=""
                            link="/feeds/[feed]" linkAs={`/feeds/${feed.id}`}
                        >
                            <ExtraInfo>
                                <Text fontSize="sm">{count(feed.packages.length, "packages")}</Text>
                            </ExtraInfo>
                            <Buttons />
                        </LinkListItem>
                    )):
                    (
                        <Text color="gray.500" as="i">There are not any feeds yet.
                            {props.canAddFeed && (
                                <span css={{marginLeft: ".25em"}}>
                                    You can <Link color="blue.300" href="/feeds/create">add one</Link>.
                                </span>
                            )}
                        </Text>
                    )
            ) : (
                <>
                    <noscript>
                        <Text>
                            Unfortunately, without Javascript enabled, we can't list the feeds here. However, you can
                            see a list through the <Link color="blue.500" href="/api/feeds">api</Link>.
                        </Text>
                    </noscript>

                    {status !== "error" ? Array.from({length: 3}, (_, i) => (
                        <Skeleton mt={4} key={i}>
                            <LinkListItem title="Should be hidden lol" detail="maybe u look in html?" link="#" description="">
                                <Buttons />
                            </LinkListItem>
                        </Skeleton>
                    )) : (
                        <Stack isInline align="center">
                            <Icon name="warning" color="red.500" size="6" />
                            <Text>Oops! Something went wrong. Please try again later.</Text>
                        </Stack>
                    )}
                </>
            )}

            {props.canAddFeed && (
                <Text d="block" mt={6}>Or, <Link color="blue.500" href="/feeds/create">add a new feed</Link>.</Text>
            )}
        </Box>
    );
}

export const getServerSideProps: GetServerSideProps<Props> = async ctx => {
    const api: GitlabApi = inject(ctx, GitlabApi);

    return {
        props: {
            canAddFeed: api === null ? false : await api.getGroups({
                min_access_level: AccessLevel.Owner
            }).then(list => list.length > 0)
        }
    };
};
