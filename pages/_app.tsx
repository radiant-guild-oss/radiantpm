import {AppProps} from "next/app";
import React, {FC, useCallback} from "react";
import {destroyCookie, parseCookies} from "nookies";
import {Box, CSSReset, Divider, Spinner, ThemeProvider} from "@chakra-ui/core";
import {useRouter} from "next/router";
import {useAsync} from "@react-hook/async";
import "../assets/space-grotesk/index.css";
import {Header, MenuItem} from "../components/Header";
import GitlabApi from "../lib/GitlabApi";
import isLoggedIn from "../lib/isLoggedIn";
import setupServices from "../lib/setupServices";
import settings from "../lib/settings";
import Head from "next/head";

if (!process.browser) setupServices();

const App: FC<AppProps> = ({Component, pageProps}) => {
    const cookies = parseCookies();

    const [{status: loggedInStatus, value: loggedIn}, fetchLoggedIn] = useAsync(async () => {
        if (!isLoggedIn(cookies)) return false;
        return await GitlabApi.isValidAccessToken(cookies.accessToken);
    });

    if (loggedInStatus === "idle") fetchLoggedIn();

    return (
        <div>
            <Head>
                <title>{settings.name}{settings.branding ? " :: RadiantPM" : ""}</title>
            </Head>

            <Header title={settings.name} titleLink="/">
                <MenuItem href="/feeds">Feeds</MenuItem>
                {loggedIn !== false && <Divider orientation="vertical"/>}
                {loggedIn ? (
                    <MenuItem href="/api/login-flow/logout" query={{redirect: location.href}}>Log out</MenuItem>
                ) : loggedInStatus === "loading" && (
                    <Spinner />
                )}
            </Header>

            <Box as="main" width={["full", "30em", "40em", "60em"]} margin="0 auto" padding={[4, "1rem 0"]}>
                <Component {...pageProps} />
            </Box>
        </div>
    );
};

export default function RadiantPM(props: AppProps) {
    return (
        <ThemeProvider>
            <CSSReset />
            <App {...props} />
        </ThemeProvider>
    )
}
