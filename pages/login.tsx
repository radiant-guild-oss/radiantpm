import React, {ChangeEvent, FC, useCallback, useEffect, useMemo, useState} from "react";
import {GetServerSideProps} from "next";
import {useRouter} from "next/router";
import {parseCookies, setCookie} from "nookies";
import {
    Box,
    FormControl,
    FormHelperText,
    FormLabel,
    Heading,
    Input,
    InputGroup, InputRightElement,
    Spinner,
    Text,
    Tooltip
} from "@chakra-ui/core";
import isLoggedIn from "../lib/isLoggedIn";
import GitlabApi from "../lib/GitlabApi";
import {Diagnostic, isValid, Severity, Validator} from "../lib/controlled-input";
import Link from "../components/Link";

const AlreadyLoggedIn: FC = () => {
    const router = useRouter();
    const redirectUrl = router.query["redirect"]?.toString() || "/";

    useEffect(() => {
        router.replace(redirectUrl);
    });

    return (
        <p>Redirecting to <Link replace href={redirectUrl}>{redirectUrl}</Link>...</p>
    );
};

const accessTokenValidator: Validator<string> = val => {
    const diagnostics: Diagnostic[] = [];

    if (/[^a-zA-Z0-9_-]/.test(val)) diagnostics.push({
        message: "Invalid characters",
        type: "characters",
        severity: Severity.Error
    });

    if (val.length !== 20) diagnostics.push({
        message: "Must be 20 characters long",
        type: "length",
        severity: Severity.Error
    });

    return diagnostics;
};

const LogIn: FC<LoginProps> = props => {
    const [accessToken, setAccessToken] = useState("");
    const [{tokenCheckResult, tokenCheckLoading}, setTokenCheck] =
        useState({tokenCheckResult: false, tokenCheckLoading: false});
    const router = useRouter();

    useEffect(() => {
        if (!isValid(accessTokenValidator, accessToken)) return;

        setTokenCheck({
            tokenCheckLoading: true,
            tokenCheckResult: false
        });

        const api = new GitlabApi(accessToken);

        api.checkAccessToken().then(res => setTokenCheck({
            tokenCheckResult: res,
            tokenCheckLoading: res
        }));
    }, [accessToken]);

    useEffect(() => {
        if (!tokenCheckResult) return;
        setCookie(null, "accessToken", accessToken, {
            maxAge: 60 * 60 * 24 * 7, // 1wk
            path: "/",
            sameSite: "strict"
        });

        if (props.redirect) location.assign(props.redirect);
        else router.push("/");
    }, [tokenCheckResult])

    const completeAccessTokenValidator = useCallback<Validator<string>>(val => {
        const diagnostics: Diagnostic[] = [];

        if (val.length === 20 && !tokenCheckResult && !tokenCheckLoading) diagnostics.push({
            message: "Invalid access token",
            severity: Severity.Error,
            type: "invalid"
        });

        return [
            ...accessTokenValidator(val),
            ...diagnostics
        ];
    }, [tokenCheckResult, tokenCheckLoading]);

    const accessTokenDiagnostics = useMemo(() => completeAccessTokenValidator(accessToken), [accessToken]);

    return (
        <Box>
            <Heading size="md">Log In</Heading>

            <FormControl
                isInvalid={accessTokenDiagnostics.length > 0}
            >
                <FormLabel htmlFor="access-token">Access token</FormLabel>
                <Tooltip
                    aria-label="Invalid" label={accessTokenDiagnostics[0]?.message}
                    isOpen={accessTokenDiagnostics.length > 0}
                    hasArrow
                >
                    <Box>
                        <InputGroup>
                            <Input
                                id="access-token"
                                value={accessToken}
                                onChange={(e: ChangeEvent<HTMLInputElement>) => setAccessToken(e.target.value)}
                            />
                            {tokenCheckLoading && (
                                <InputRightElement>
                                    <Spinner />
                                </InputRightElement>
                            )}
                        </InputGroup>
                    </Box>
                </Tooltip>
                <FormHelperText>
                    Should have read_user and read_api scopes.
                </FormHelperText>
            </FormControl>

            <Box borderWidth="1px" rounded="md" padding={2} marginTop={8} color="gray.600">
                <Text>
                    You can get an access token at&nbsp;
                    <Link href="https://gitlab.com/profile/personal_access_tokens" target="_blank" color="blue.500">
                        https://gitlab.com/profile/personal_access_tokens
                    </Link>
                </Text>
            </Box>
        </Box>
    );
};

interface LoginProps {
    loggedIn: boolean;
    redirect?: string;
}

export default (props: LoginProps) => {
    if (props.loggedIn) return <AlreadyLoggedIn />;
    return <LogIn {...props} />;
};

export const getServerSideProps: GetServerSideProps = async ctx => {
    const cookies = parseCookies(ctx);

    const loggedIn = isLoggedIn(cookies) && await GitlabApi.isValidAccessToken(cookies.accessToken);
    const redirect = ctx.query.redirect as string || null;

    return {
        props: {
            loggedIn,
            redirect
        }
    };
};
