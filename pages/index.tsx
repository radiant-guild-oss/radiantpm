import Head from "next/head";
import React from "react";
import {GetServerSideProps} from "next";
import {parseCookies} from "nookies";
import GitlabApi from "../lib/GitlabApi";
import isLoggedIn from "../lib/isLoggedIn";

export default function Home() {
    return (
        <div className="container">
            <Head>
                <title>RadiantPM</title>
            </Head>


        </div>
    )
}

export const getServerSideProps: GetServerSideProps = async ctx => {
    const cookies = parseCookies(ctx);

    if (!isLoggedIn(cookies) || !await GitlabApi.isValidAccessToken(cookies.accessToken)) {
        ctx.res.statusCode = 301;
        ctx.res.setHeader("Location", "/login");
    }

    return {
        props: {}
    };
};
