import React, {FC,} from "react";
import {Box, Flex, Heading, PseudoBox, Text} from "@chakra-ui/core";
import Link from "./Link";

export interface HeaderProps {
    title: string;
    titleLink: string;
}

interface MenuItemProps {
    href: string;
    query?: Record<string, string>;
    onClick?(): void;
}

export const MenuItem: FC<MenuItemProps> = ({href, query, children, ...props}) => (
    <PseudoBox
        as={Text}
        mt={{base: 4, md: 0}} mr={3} ml={3}
        display="block"
        fontFamily="Space Grotesk"
        _hover={{opacity: .9}}
        _active={{opacity: .6}}
    >
        <Link href={href} query={query} onClick={props.onClick}>{children}</Link>
    </PseudoBox>
);

export const Header: FC<HeaderProps> = props => {
    return (
        <Box
            as="nav"
            bg="blue.700"
            color="white"
        >
            <Flex
                width={["full", "30em", "40em", "60em"]} margin="0 auto" padding={[4, "1rem 0"]}
                justify="space-between" align="center"
            >
                <Heading
                    size="lg"
                    fontFamily="Space Grotesk"
                    fontWeight={700}
                >
                    <Link href={props.titleLink}>{props.title}</Link>
                </Heading>

                <Box
                    display={{sm: "none", md: "flex"}}
                    width="auto"
                    marginLeft={6}
                    alignItems="stretch"
                    flexGrow={1}
                >
                    {props.children}
                </Box>
            </Flex>
        </Box>
    );
};
